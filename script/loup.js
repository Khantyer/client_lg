var WS;
var PSEUDO;
var STATE;

$(document).ready(
        function () {
            WS = new WebSocket('ws://127.0.0.1:8000/');
            WS.onopen = function (event) {
                var data = $.parseJSON(event.data);
                for (var i = 0; i < data.length; i++) {
                    $("#liste_joueur").append("<li>" + data[i] + "</li>");
                }
            };
            WS.onmessage = function (event) {
                var data = $.parseJSON(event.data);
                switch (data.Type) {

                    case -1:
                        switch (data.Erreur) {
                            case 0:
                                $("#connexion").hide();
                                alert("La partie est déjà en cours");
                                break;
                            case 1:
                                errEmptyPseudo();
                                break;
                            case 2:
                                $("#helpIdError").text("Le pseudo n'est pas disponible");
                                $("#username-field").addClass("has-error");
                                break;
                        }
                        break;

                    case 0:
                        $("#listeDisponible").empty();
                        $("#listeDisponible").append("<tr><th>Nom</th><th>Slot</th><th>Rejoindre</th></tr>");
                        $("#listeIndisponible").empty();
                        $("#listeIndisponible").append("<tr><th>Nom</th><th>Slot</th><th>Observer</th></tr>");

                        for (var i = 0; i < data.ListePartie.length; i++) {
                            var Partie = data.ListePartie[i];
                            if (Partie.Etat === -1) {
                                $("#listeDisponible").append("<tr><td>" + Partie.Name + "</th><td>" + Partie.NombreJoueur + "/" + Partie.Slot + "</td><td><button class='btn btn-success' type='submit' onclick='joinGame(" + i + ")'>Rejoindre</button></td></tr>");
                            } else {
                                $("#listeDisponible").append("<tr><td>" + Partie.Name + "</th><td>" + Partie.NombreJoueur + "/" + Partie.Slot + "</td><td><button class='btn btn-success' type='submit' onclick='joinGame(" + i + ")'>Observer</button></td></tr>");
                            }
                        }
                        break;

                    case 1:
                        $('#FenetreSelection').modal("hide");
                        $('#FenetreCreationPartie').modal("hide");
                        STATE = data.Etat;
                        if (STATE !== -1) {
                            $("#connexion").hide();
                        }
                        for (var i = 0; i < data.ListeJoueur.length; i++) {
                            var joueur = data.ListeJoueur[i];
                            if (joueur.Founder)
                            {
                                $("#liste_joueur").append("<li id='" + joueur.Pseudo + "'>" + joueur.Pseudo + " ★</li>");
                            } else {
                                $("#liste_joueur").append("<li id='" + joueur.Pseudo + "'>" + joueur.Pseudo + "</li>");
                            }
                        }
                        break;

                    case 2:
                        if (data.Pseudo === PSEUDO) {
                            $("#message-field").prop("disabled", false);
                            $("#buttonSendChat").prop("disabled", false);
                            Spectator = false;
                            $("#connexion").hide();
                        }

                        $("#liste_joueur").append("<li id='" + data.Pseudo + "'>" + data.Pseudo + "</li>");
                        $("#chat_output").append("<p>" + data.Pseudo + " a rejoint la partie</p>");
                        $("#chat_output").scrollTop($("#chat_output").prop("scrollHeight"));
                        break;

                    case 3:
                        $("#chat_output").append("<p><a>" + data.Pseudo + "</a> : " + data.Message + "</p>");
                        $("#chat_output").scrollTop($("#chat_output").prop("scrollHeight"));
                        break;

                    case 4:
                        $("#" + data.Pseudo).remove();
                        $("#chat_output").append("<p>" + data.Pseudo + " a quitté la partie</p>");
                        break;

                    case 5:
                        $("#" + data.Pseudo).append(" ★");
                        $("#chat_output").append("<p>" + data.Pseudo + " est désormais le leader</p>");
                        break;
                }
            };

            $("#chat_input").submit(
                    function (event) {
                        var message = $("#message-field").val();
                        if (message === "") {
                            return false;
                        }
                        var obj = new Object();
                        obj.Type = 4;
                        obj.Message = message;
                        sendData(obj);
                        event.preventDefault();
                    }
            );

            $("#identification").submit(
                    function (event) {
                        var pseudo = $("#username").val();
                        if (pseudo === "") {
                            errEmptyPseudo();
                            return false;
                        }
                        PSEUDO = pseudo;
                        var obj = new Object();
                        obj.Type = 3;
                        obj.Pseudo = pseudo;
                        sendData(obj);
                        event.preventDefault();
                    }
            );

            $("#CreationPartieForm").submit(
                    function (event) {
                        var Nom = $("#NomPartie").val();
                        if (Nom === "") {
                            errEmptyGameName();
                            return false;
                        }
                        var obj = new Object();
                        obj.Type = 1;
                        obj.NomPartie = Nom;
                        obj.Slot = $("#NombreSlot").val();
                        sendData(obj);
                        event.preventDefault();
                    }
            );

            $("#ButtonCreationPartie").click(
                    function () {
                        $('#FenetreSelection').modal('hide');
                        $('#FenetreCreationPartie').modal('show');
                    }
            );

            $("#ButtonAbortCreation").click(
                    function () {
                        $('#FenetreCreationPartie').modal('hide');
                        $('#FenetreSelection').modal('show');
                    }
            );

            $("#message-field").prop("disabled", true);
            $("#buttonSendChat").prop("disabled", true);
            $('#FenetreSelection').modal("show");
        }
);
function sendData(data) {
    WS.send(JSON.stringify(data));
}

function errEmptyPseudo() {
    $("#helpIdError").text("Le champ pseudo ne peut pas être vide");
    $("#username-field").addClass("has-error");
}

function joinGame(id) {
    var obj = new Object();
    obj.Type = 2;
    obj.Id = id;
    sendData(obj);
}

function errEmptyGameName() {
    $("#helpGameNameError").text("Le nom de la partie ne peut être vide");
    $("#NomPartieDiv").addClass("has-error");
}